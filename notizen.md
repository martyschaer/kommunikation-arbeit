# Fragestellung
Sind Informatiker eher introvertiert oder extravertiert und schätzen sie sich korrekt ein?

# Links
- Umfrage: https://docs.google.com/forms/d/1I3o2Uq-whA-RRC5oA9iIFzioLaQXyBgpRBvQMyDT1ls/edit?usp=sharing
- McCroskey: http://www.jamescmccroskey.com/measures/introversion.htm
- https://www.sciencedirect.com/science/article/abs/pii/S0092656615300052
- https://arstechnica.com/science/2015/11/programmers-are-a-tiny-bit-introverted-but-otherwise-agreeable/?comments=1

# Rahmenbedingungen
- Informatiker

# Methodik
- Umfrage
  * Selbsteinschätzung
  * Fragen zu Bestimmung I/E
  * Grösse der Firma
  * Altersgruppen
  * Geschlecht
  * Aufwachsen / akt. Wohnsitz [Stadt, Aglomeration, Land]
  * Rolle
- Beobachtung von Mitarbeitenden in der Firma
  * Selbsteinschätzung
  * Verhalten in Meetings, Präsentationen, Telefon
- Experimente mit unvorbereiteter Präsentation (Kontrollgruppe/Informatiker)

# Gespräch 1
- Kontrollgruppe ist gut
- Einleitung zu kurz
  - Zweck Warum herausfinden?
    - Informatiker brauchen Unterstützung?
  - Stand der Wissenschaft
    - Was ist Big Five (kurze Zusammenfassung)
      - (Marketing)
  - Projektablauf zu kurz
    - mehr schreiben
- Verfahren und Methoden
  - Kontrollgruppen sind keine Methoden
  - Interviews wegkorrigieren
  - Experimente
    - Sitzung simulieren
    - 

- Ablauf
  - Je Zwischenfazit nach schritten
  - Vorarbeit
    - Literatur
    - Big5 erklären
    - Umfrage beschreiben
  - Online Umfrage
  - Experimente
    - neue Kaffeemaschine
    - Vorschläge Sammeln
    - 3 Möglichkeiten
    - gleiches Thema bei Kontrollgruppen/IT
  - Endergebnis
  - Schlussfolgerung

# Gespräch 2
- Kontrollgruppe ist gut ✓
- Gut vorbereitete Umfragen ✓
- Zweck genauer beschreiben (1.2)
  * persönliches Interesse?
- Ausgangslage fehlt
  * Informatiker haben ein intravertiertes Image
- References (kein "siehe Quellenverzeichnis", evt. kurztitel) ✓
- "wollen wir" -> die Arbeit zeigt (Präsens)
- 3.2 sollte ausführlicher sein
- Struktur der Befragen und Kontrollgruppen ✓
  * Demographie genau beschreiben
  * für repeatability
- Myers Briggs Primärliteratur ✓

Weitere Studien:
https://www.computerwoche.de/a/informatiker-sind-die-introvertierteste-berufsgruppe,2885026,2
