# Intro- & Extraversion in the IT industry
This is a paper and presentation about intro- and extraversion in the IT industry.

## Files
* [Paper](https://gitlab.com/martyschaer/kommunikation-arbeit/builds/artifacts/master/raw/Arbeit.pdf?job=PDF)
* [Presentation](https://gitlab.com/martyschaer/kommunikation-arbeit/builds/artifacts/master/raw/Präsentation.pdf?job=PDF)
